# multi worm thingy

A very simple multiworm tracker written in `C++`.

### Prerequisites

`C++` compiler with support for `C++11` (e.g. GNU C++ Compiler version 4.8.5 or above),
`CMake` version 2.8 or above,
GNU `make`.

### Synopsis

  bg_create input output [options]

    create background image output (extension determines image format)
     from video input. c.f. source code for more options.

  multi_worm_thingy input background [options]

     track worms in input video, subtract background before processing.
     see source for more options.

## Authors

* Thomas Ranner, T.Ranner at Leeds.ac.uk, Room 9.10h, EC Stoner Building, University of Leeds, Leeds. LS2 9JT. UK.
* Robert I. Holbrook.

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details.

    Copyright (C) 2017 Thomas Ranner, Robert I. Holbrook

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


