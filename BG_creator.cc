// Copyright (C) 2017 Thomas Ranner, Robert I. Holbrook

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "opencv2/opencv.hpp"

#include <cassert>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <utility>
#include <vector>
#include <string>

using namespace cv;

struct CommandLineArgumentParser
{
  CommandLineArgumentParser( int argc, char** argv )
  {
    if( not ( argc >= 3 ) )
      {
	usage( argv );
	abort();
      }

    inputVideo = argv[1];
    outputImage = argv[2];

    // set defaults
    method = "mean";
    methodCase = 0;
    startFrame = 0;
    maxFrame = -1;
    frameStep = -1;

    // over write from parameters
    for( int i = 3; i < argc; ++i )
      {
	std::istringstream is_line( argv[i] );
	std::string key;
	if( std::getline( is_line, key, ':' ) )
	  {
	    std::string value;
	    if( std::getline( is_line, value ) )
	      {
		if( key.compare( "method" ) == 0 )
		  {
		    if( value.compare( "mean" ) == 0 )
		      {
			method = "mean";
			methodCase = 0;
		      }
		    else if( value.compare( "max" ) == 0 )
		      {
			method = "max";
			methodCase = 1;
		      }
		    else if( value.compare( "min" ) == 0 )
		      {
			method = "min";
			methodCase = 2;
		      }
		    else
		      {
			std::cerr << "unknown method type. choose from:\n"
				  << " mean, max, min" << std::endl;
			abort();
		      }
		  }
		else if( key.compare( "startframe" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> startFrame;
		  }
		else if( key.compare( "maxframe" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> maxFrame;
		  }
		else if( key.compare( "framestep" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> frameStep;
		  }
		else
		  {
		    std::cerr << "unknown parameter key. choose from:\n"
			      << " method, maxframe, framestep" << std::endl;
		    abort();
		  }
	      }
	  }
      }

    std::cout << "- input video: " << inputVideo << "\n"
	      << "- output image: " << outputImage << "\n"
	      << "- method: " << method << " (" << methodCase << ")" << "\n"
	      << "- start frame: " << startFrame << "\n"
	      << "- max frame: " << maxFrame << "\n"
	      << "- frame step: " << frameStep << std::endl;
  }

  void usage( char** argv ) const
  {
    std::cout << "usage: " << std::endl;
    std::cout << " " << argv[0] << " {input video} {output background image} {..options..}" << std::endl;
  }

  std::string inputVideo;
  std::string outputImage;

  std::string method;
  int methodCase;
  int startFrame;
  int maxFrame;
  int frameStep;
};

void silhoutteImage( const Mat& input, const Mat& background, Mat& output, const double minValue )
{
  // subtract background image
  output = background - input;

  // set values less than minValue to 0
  threshold( output, output, minValue, 255, THRESH_TOZERO );

  // find threshold to binary using Otsu's method
  threshold( output, output, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU );
}

void invert( Mat& mat )
{
  // invert each pixel in mat
  for( int i = 0; i < mat.rows; ++i)
    {
      uchar* p = mat.ptr<uchar>(i);

      for ( int j = 0; j < mat.cols; ++j)
	{
	  const int pj = 255-(int) p[j];
	  p[j] = (uchar) pj;
	}
    }
}

void algorithm( const std::string videoFn, const std::string BG_image,
		const int methodCase, const int startFrame,
		const int maxFrame, const int frameStep )
{
  // extract file name info
  std::cout << "using video stream " << videoFn << std::endl;

  // open video stream
  VideoCapture cap( videoFn );

  // check if we succeeded
  if(!cap.isOpened())
    {
      throw "video capture not opened";
    }

  std::cout << videoFn << " opened successfully." << std::endl;

  // frame loop
  std::clock_t start;
  start = std::clock();

  int nextRead = ( startFrame >= 0 ) ? startFrame : 0;

  Mat acc;
  int count;
  bool firstRead = true;
  for( count = 1; ( maxFrame >= 0 ) ? ( count < maxFrame ) : true; ++count )
    {
      if( count % 100 == 0 )
	std::cout << "frame: " << count << std::endl;

      // read frame
      Mat frame;
      if( count >= nextRead )
	{
	  cap >> frame;
	  nextRead += frameStep;
	}
      else
	{
	  cap.grab();
	  continue;
	}

      if( frame.empty() )
	{
	  std::cout << "empty frame" << std::endl;
	  break;
	}

      // convert to grayscale
      Mat gray;
      cvtColor( frame, gray, CV_BGR2GRAY);

      if( firstRead )
      	{
      	  // create copy
	  if( methodCase == 0 )
	    gray.convertTo( acc, CV_64F );
	  else
	    gray.copyTo( acc );

	  // first read complete
	  firstRead = false;
      	}
      else
      	{
	  switch( methodCase )
	    {
	    case 0: // mean
	      accumulate( gray, acc );
	      break;
	    case 1: // max
	      max( gray, acc, acc );
	      break;
	    case 2: // min
	      min( gray, acc, acc );
	      break;
	    }
      	}
#if DISPLAY
      cv::Mat twin;
      if( methodCase == 0 )
	{
	  cv::Mat grayDbl;
	  gray.convertTo( grayDbl, acc.type() );
	  cv::hconcat( grayDbl, acc, twin );
	}
      else
	{
	  cv::hconcat( gray, acc, twin );
	}
      cv::imshow("bg creator", twin);
      cv::waitKey(10);
#endif
    }

  // convert and write out
  Mat ttt = ( methodCase == 0 ) ? ( acc / count ) : acc;
  ttt.convertTo( ttt, CV_8U );
  imwrite( BG_image, ttt );

  std::cout << "background image written to " << BG_image << std::endl;

  // print run time
  const double totalTime = (std::clock() - start) / (double)(CLOCKS_PER_SEC);
  std::cout << "total time " << totalTime << " sec." << std::endl;
}

int main(int argc, char** argv)
{
#if DISPLAY
  cv::namedWindow( "bg creator", WINDOW_NORMAL );
  cv::resizeWindow( "bg creator", 2048, 1024 );
#endif

  CommandLineArgumentParser parser( argc, argv );

  try {
    algorithm( parser.inputVideo, parser.outputImage, parser.methodCase,
	       parser.startFrame, parser.maxFrame, parser.frameStep );
    return 0;
  }
  catch( const char* msg ) {
    std::cerr << "Exception: " << msg << std::endl;
    return 1;
  }
}
