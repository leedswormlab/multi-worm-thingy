// Copyright (C) 2017 Thomas Ranner, Robert I. Holbrook

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "opencv2/opencv.hpp"

#include <cassert>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <utility>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <limits>

#include <sys/types.h>
#include <sys/stat.h>

using namespace cv;

struct CommandLineArgumentParser
{
  CommandLineArgumentParser( int argc, char** argv )
  {
    if( not ( argc >= 3 ) )
      {
	usage( argv );
	abort();
      }

    inputVideo = argv[1];
    bgImage = argv[2];

    // set defaults
    method = "mean";
    methodCase = 0;
    startFrame = 0;
    maxFrame = -1;
    frameStep = -1;
    minArea = 0.0;
    maxArea = std::numeric_limits<double>::max();

    // over write from parameters
    for( int i = 3; i < argc; ++i )
      {
	std::istringstream is_line( argv[i] );
	std::string key;
	if( std::getline( is_line, key, ':' ) )
	  {
	    std::string value;
	    if( std::getline( is_line, value ) )
	      {
		if( key.compare( "method" ) == 0 )
		  {
		    if( value.compare( "mean" ) == 0 )
		      {
			method = "mean";
			methodCase = 0;
		      }
		    else if( value.compare( "max" ) == 0 )
		      {
			method = "max";
			methodCase = 1;
		      }
		    else if( value.compare( "min" ) == 0 )
		      {
			method = "min";
			methodCase = 2;
		      }
		    else
		      {
			std::cerr << "unknown method type. choose from:\n"
				  << " mean, max, min" << std::endl;
			abort();
		      }
		  }
		else if( key.compare( "startframe" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> startFrame;
		  }
		else if( key.compare( "maxframe" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> maxFrame;
		  }
		else if( key.compare( "framestep" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> frameStep;
		  }
		else if( key.compare( "maxarea" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> maxArea;
		  }
		else if( key.compare( "minarea" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> minArea;
		  }
		else
		  {
		    std::cerr << "unknown parameter key. choose from:\n"
			      << " method, startframe, maxframe, framestep, minarea, maxarea" << std::endl;
		    abort();
		  }
	      }
	  }
      }

    std::cout << "- input video: " << inputVideo << "\n"
	      << "- bg image: " << bgImage << "\n"
	      << "- method: " << method << " (" << methodCase << ")" << "\n"
	      << "- start frame: " << startFrame << "\n"
	      << "- max frame: " << maxFrame << "\n"
	      << "- frame step: " << frameStep << "\n"
	      << "- min area: " << minArea << "\n"
	      << "- max area: " << maxArea << std::endl;
  }

  void usage( char** argv ) const
  {
    std::cout << "usage: " << std::endl;
    std::cout << " " << argv[0] << " {input video} {background image} {..options..}" << std::endl;
  }

  std::string inputVideo;
  std::string bgImage;

  std::string method;
  int methodCase;
  int startFrame;
  int maxFrame;
  int frameStep;
  double maxArea;
  double minArea;
};

void getBackgroundImage( const std::string& fn, Mat& output )
{
  output = imread( fn, CV_LOAD_IMAGE_GRAYSCALE );

  if( output.empty() )
    throw "unable to open background image";
}

void silhoutteImage( const Mat& input, const Mat& background, Mat& output, const double minValue )
{
  // subtract background image
  output =  input - background;

  // set values less than minValue to 0
  threshold( output, output, minValue, 255, THRESH_TOZERO );

  // find threshold to binary using Otsu's method
  threshold( output, output, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU );
}

void myFindContours( const Mat& /*gray*/, const Mat& input,  std::vector<std::vector<cv::Point> >& contours,
		     const double minArea, const double maxArea )
{
  // find contours
  std::vector<cv::Vec4i> hierarchy;
  findContours( input, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE );

  // erase if too big or too small
  contours.erase( std::remove_if( contours.begin(),
				  contours.end(),
				  [&]( const std::vector<cv::Point>& contour ) -> bool
				  {
				    const double area = cv::contourArea( contour );
				    return area < minArea or area > maxArea;
				  }), contours.end() );

 #if DISPLAY
  Mat output;
  cvtColor( gray, output, CV_GRAY2BGR);
  drawContours( output, contours, -1, Scalar(0,0,255), 2, 8, hierarchy, 0, Point() );
#endif
}

void meanContour( const std::vector<cv::Point>& contour, cv::Point2d& mean )
{
  mean.x = 0.0;
  mean.y = 0.0;

  for ( auto pt : contour )
  {
    mean.x += pt.x;
    mean.y += pt.y;
  }

  mean.x /= (double) contour.size();
  mean.y /= (double) contour.size();
}
void allWorms( const std::vector<std::vector<cv::Point> >& contours, std::vector<Point2d>& coordinates )
{
  for ( auto contour : contours)
    {
      Point2d pt;
      meanContour( contour, pt );
      coordinates.push_back(pt);
    }
}
void save( const std::vector<Point2d>& coordinates, const std::string& filename )
{
  std::ofstream myfile;
  myfile.open ( filename.c_str() );

  if( not myfile.is_open() )
    throw "Unable to open output file.";

  for ( auto pt : coordinates )
    {
      myfile << pt.x << ", " << pt.y << std::endl;
    }

  myfile.close();
}

void generateFilename( const int frame, const std::string& videoFn, std::string& filename )
{
  size_t lastindex = videoFn.find_last_of( "." );
  std::string pathname = videoFn.substr( 0, lastindex );
  struct stat st;
  // this is c code that checks whether the dirrectory exists and if not, creates it
  if (stat( pathname.c_str(), &st) == -1)
    {
      mkdir(pathname.c_str(), 0770);
      std::cout << pathname << " directory created" << std::endl;
    }
  size_t lastindex2 = videoFn.find_last_of( "/" );
  std::string rawname = videoFn.substr( lastindex2+1,
					lastindex-lastindex2-1 );
  std::stringstream ss;
  ss << pathname << "/" << rawname << "_" << std::setfill( '0' ) <<
    std::setw( 6 )<< frame  << ".csv";
  filename = ss.str();
}

void algorithm( const CommandLineArgumentParser& parameters )
{
  // extract file name info
  const std::string videoFn = parameters.inputVideo;
  std::cout << "using video stream " << videoFn << std::endl;

  // open video stream
  VideoCapture cap( videoFn );

  // check if we succeeded
  if(not cap.isOpened())
    {
      throw "video capture not opened";
    }

  std::cout << videoFn << " opened successfully." << std::endl;

  // select background image
  Mat BGimg;
  getBackgroundImage( parameters.bgImage ,BGimg );

#if DISPLAY
  cv::namedWindow( "t", WINDOW_NORMAL );
  resizeWindow( "t", 1024, 1024 );
#endif

  // extract frame loop parameters
  const int startFrame = parameters.startFrame;
  const int maxFrame = parameters.maxFrame;
  const int frameStep = parameters.frameStep;

  // frame loop
  std::clock_t start;
  start = std::clock();

  int nextRead = ( startFrame >= 0 ) ? startFrame : 0;

  int count;
  for( count = 1; ( maxFrame >= 0 ) ? ( count < maxFrame ) : true; ++count )
    {
      if( count % 100 == 0 )
	std::cout << "frame: " << count << std::endl;

      // read frame
      Mat frame;
      if( count > nextRead )
	{
	  cap >> frame;
	  nextRead += frameStep;
	}
      else
	{
	  cap.grab();
          continue;
	}

      if( frame.empty() )
	{
	  std::cout << "empty frame" << std::endl;
	  break;
	}
      // convert to grayscale
      Mat gray;
      cvtColor( frame, gray, CV_BGR2GRAY);

      // silhouette
      Mat silhouette;
      silhoutteImage(gray,BGimg,silhouette,10);

      // contours
      std::vector<std::vector<cv::Point> > contours;
      myFindContours( gray, silhouette, contours, parameters.minArea, parameters.maxArea );

      // compute mean points
      std::vector<Point2d> coordinates;
      allWorms( contours, coordinates );

      // generate filename
      std::string filename;
      generateFilename( count, videoFn, filename );

      // save the mean points
      save( coordinates, filename );

#if DISPLAY
      Mat output;
      cvtColor( gray, output, CV_GRAY2BGR);
      for( auto pt: coordinates )
        circle( output, pt, 20, Scalar(0,0,255), 4 );
      imshow( "t", output );
      waitKey(5);
#endif

      // output number of contours
      std::cout << "number of contours found: " << contours.size() << std::endl;
    }

  // print run time
  const double totalTime = (std::clock() - start) / (double)(CLOCKS_PER_SEC);
  std::cout << "total time " << totalTime << " sec." << std::endl;
}

int main(int argc, char** argv)
{
  CommandLineArgumentParser parser( argc, argv );

  try {
    algorithm( parser );
    return 0;
  }
  catch( const char* msg ) {
    std::cerr << "Exception: " << msg << std::endl;
    return 1;
  }
}
